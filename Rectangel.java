import java.util.Scanner;

public class Rectangel{
        public static Scanner scanner = new Scanner(System.in);
        
    /**
     * Main function starts the draw rectangel program. The Main function runs infinite
     * to the user writes q in the terminal. The program asks for the width and the height for
     * drawing a new rectangel, and calls on the drawRectangel or draw2Rectangels based on the size.
     * If width or height is less than 7, only one rectangular i drawed. 
     */
    public static void main(String []args){
        int width, height;

        while (true){
            System.out.println("\n\nWrite q to quit the program.");
            System.out.println("Write the width for the rectangel:");
            width = parseInput(scanner.nextLine());
            if (width == -1){
                break;
            }

            System.out.println("Write the height for the rectangel:");
            height = parseInput(scanner.nextLine());
            if (height == -1){
                break;
            }

            System.out.println("Write the character for the rectangel:");
            String character = scanner.nextLine();

            while (character.length() != 1){
                System.out.println("Only one character is allowed. Write a new character for the rectangel:");
                character = scanner.nextLine();
            }
            if (width < 7 || height < 7){
                drawRectangel(width, height, character.charAt(0));
            } else{
                draw2Rectangels(width, height, character.charAt(0));
            }
        }
        

    }

    /**
     * This function checks that the input is legal. Returns the width/ height that the user write.
     * If the user write q for quiting the program, the function returns -1.
     */
    public static int parseInput(String str){
        while (!str.equalsIgnoreCase("q") && !str.matches("[0-9]+")){
            System.out.println("Wrong input. Type q for quit the program, or an integer for width/ height.");
            str = "";
            str = scanner.nextLine();
        }
        if (str.equalsIgnoreCase("q")){
            return -1;
        }
        int size = Integer.parseInt(str);

        if (size < 3){
            System.out.println("Minimum size is 3 for drawing a rectangel. Please type in a bigger number than " + size);
            return parseInput(scanner.nextLine());
        }
        return size;
    }

    /**
     * This function draws a rectangel based on the size given as parameters with the given 
     * character, witch is also one of the parameters.
     */
    public static void drawRectangel(int width, int height, char character){
        String endLine = "" + character; // First and last line in the rectangel
        String midLine = "" + character;

        for (int w = 1; w < width-1; w++){
            endLine += character;
            midLine += " ";
        }
        endLine += character;
        midLine += character;

        System.out.println(endLine);
        for (int h = 1; h < height-1; h++){
            System.out.println(midLine);
        }
        System.out.println(endLine);
    }

    /**
     * This function draws 2 nested rectangels based on the size given as parameters with the given 
     * character, witch is also one of the parameters.
     */
    public static void draw2Rectangels(int width, int height, char character){
        for (int h = 0; h < height; h++){
            for (int w = 0; w < width; w++){
                
                if (h == 0 || h == height-1){ // First and last line
                        System.out.print(character);

                } else if (h == 1 || h == height-2){ // Second first and second last line
                    if (w == 0 || w == width-1){ // First and last char in line
                        System.out.print(character);
                    } else{
                        System.out.print(" ");
                    }

                } else if (h == 2 || h == height-3){ // Third first and third last line
                    if (w == 1 || w == width-2){ // Second first and second last char in line
                        System.out.print(" ");
                    } else{
                        System.out.print(character);
                    }

                } else {  // Other lines
                    // Writes at first place, second place, last place, second last place
                    if (w == 0 || w == 2 || w == width-1 || w == width-3){ 
                        System.out.print(character);
                    } else{
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}